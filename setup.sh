#!/bin/bash

# initial set up of server: sets hostname, sudo user, port, firewall, and brute login protection
#usage sh setup.sh hostname username

HOST=$1
USER=$2
IPADDRESS=$(hostname -I | cut -d " " -f 1)

hostnamectl set-hostname $HOST
echo "${IPADDRESS} ${HOST}.vasudevaserver.net $HOST" >> /etc/hosts
echo "Hostname set to $HOST for current ip ${IPADDRESS}"

timedatectl set-timezone America/New_York
echo "Timezone set to NY"

adduser $USER
PASS=$(</dev/urandom tr -dc A-Za-z0-9 | head -c 32)
echo $USER:$PASS | chpasswd
usermod -aG wheel $USER

echo "** Checking for version upgrades **"
yum -y update
echo "** CentOS updated to latest version **"

sed "s/^#Port .*/Port 227/i" -i /etc/ssh/sshd_config
sed "s/^#PermitRootLogin .*/PermitRootLogin no/i" -i /etc/ssh/sshd_config
sudo systemctl restart sshd
echo "SSH port reset, root login disabled and SSH restarted"

systemctl start firewalld
firewall-cmd --permanent --remove-service=ssh
firewall-cmd --permanent --add-port=227/tcp
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
firewall-cmd --reload
systemctl enable firewalld
echo "Set firewall to allow ssh, http, https"

yum -y install git
echo "Installed Git"

# install extra package manager, need advanced version for php5.5
wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-2.noarch.rpm
yum install -y epel-release-7-2.noarch.rpm
echo "Installed yum extra packages in order to get fail2ban, nginx, php5.5

yum -y install fail2ban
echo "Installed fail2ban with default config"

echo "Note : Sudo user ${USER} created with password ${PASS}"