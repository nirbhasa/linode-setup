
# sets up LEMP stack, and transfers our custom config files to their positions
# note that epel package manager needed for nginx and php5.5 is already installed in setup script

#nginx
yum -y install nginx
echo "Nginx installed"
mv /etc/nginx /etc/nginx.orig
cp -R lemp-conf/perusio /etc/nginx
rm -f /etc/nginx/nginx.conf
cp lemp-conf/nginx.conf /etc/nginx/nginx.conf
rm -f /etc/nginx/apps/drupal/drupal_boost.conf
cp lemp-conf/drupal_boost.conf /etc/nginx/apps/drupal/drupal_boost.conf
mkdir /var/cache/nginx
mkdir /etc/nginx/sites-enabled
systemctl start nginx.service
systemctl enable nginx.service

# mariadb
yum -y install mariadb-server mariadb
mv /etc/my.conf.d/server.cnf /etc/my.conf.d/server.cnf.bak
cp /usr/share/mysql/my-innodb-heavy-4G.cnf /etc/my.conf.d/server.cnf

systemctl start mariadb
DATABASE_PASS=$(</dev/urandom tr -dc A-Za-z0-9 | head -c 32)
mysqladmin -u root password "$DATABASE_PASS"
mysql -u root -p"$DATABASE_PASS" -e "UPDATE mysql.user SET Password=PASSWORD('$DATABASE_PASS') WHERE User='root'"
mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.user WHERE User=''"
mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
mysql -u root -p"$DATABASE_PASS" -e "DROP DATABASE test"
mysql -u root -p"$DATABASE_PASS" -e "FLUSH PRIVILEGES"
echo "MariaDB installed and secured"

#php 5.5
wget http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
yum install remi-release-7.rpm

yum --enablerepo=remi,remi-php55 install php php-fpm php-mysql

#php-fpm
mv /etc/php-fpm.d/www.conf /etc/php-fpm.d/www.conf.orig
cp lemp-conf/php-fpm_www.conf /etc/php-fpm.d/www.conf
systemctl start php-fpm
systemctl enable php-fpm.service

# drush requires composer, needs soft php config
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# install drush
# also need version of ini file with open_basedir and disabled_functions off
git clone https://github.com/drush-ops/drush.git /usr/share
chmod u+x /usr/share/drush/drush
ln -s /usr/share/drush/drush /usr/bin/drush
alias drush=/usr/share/drush/drush

mkdir /etc/drush
cp lemp-conf/php-drush.ini /etc/drush/php.ini
cd /usr/share/drush
composer install
cd

# harden php config
mv /etc/php.ini /etc/php.ini.orig
cp lemp-conf/php.ini /etc/php.ini
systemctl restart php-fpm

echo "Note: root MariaDB pwd ${DATABASE_PASS}""