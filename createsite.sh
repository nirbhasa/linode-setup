#!/bin/bash

# Creates Nginx vhost, home folder, adds mariadb database and user, and adds home folder to open_basedir
# usage sh createsite.sh domainname , where domainname is the name without www at beginning (eg srichinmoyraces.org)
# if you dont run script with domainname, you will be prompted for it

#config
web_root='/home/'
config_dir='/etc/nginx/sites-available'
enable_dir='/etc/nginx/sites-enabled'
nginx_dir='/etc/nginx'
IPv6ADDRESS=$(hostname -I | cut -d " " -f 2)

if [ -z "$1" ]
then
    #user input
    echo -e "Enter domain name:"
    read DOMAIN
    echo "Creating Nginx domain settings for: $DOMAIN"

    if [ -z "$DOMAIN" ]
    then   
        echo "Domain required"
        exit 1
    fi
fi

if [ -z "$DOMAIN" ]
then
    DOMAIN=$1
fi

(
cat <<EOF
server {
    listen 80; # IPv4
    listen [${IPv6ADDRESS}]:80 ipv6only=on;

    server_name *.$DOMAIN;
    limit_conn arbeit 32;

    ## Access and error logs.
    access_log /var/log/nginx/${DOMAIN}_access.log;
    error_log /var/log/nginx/${DOMAIN}_error.log;

    ## Block SQL injections
    set \$block_sql_injections 0;
    if (\$query_string ~ "union.*select.*\(") {
        set \$block_sql_injections 1;
    }
    if (\$query_string ~ "union.*all.*select.*") {
        set \$block_sql_injections 1;
    }
    if (\$query_string ~ "concat.*\(") {
        set \$block_sql_injections 1;
    }
    if (\$block_sql_injections = 1) {
        return 403;
    }

    ## Block spam
    if (\$query_string ~ "\b(lipitor|phentermin|pro[sz]ac|sandyauer|tramadol|troyhamby|mbien|blue\spill|cialis|cocaine|ejaculation|erectile|erections|hoodia|huronriveracres|impotence|levitra|libido|ultram|unicauca|valium|viagra|vicodin|xanax|ypxaieo|handbags|ugg|vuitton)\b") {
        return 403;
    }

    ## Block file injections
    set \$block_file_injections 0;
    if (\$query_string ~ "[a-zA-Z0-9_]=http://") {
        set \$block_file_injections 1;
    }
    if (\$query_string ~ "[a-zA-Z0-9_]=(\.\.//?)+") {
        set \$block_file_injections 1;
    }
    if (\$query_string ~ "[a-zA-Z0-9_]=/([a-z0-9_.]//?)+") {
        set \$block_file_injections 1;
    }
    if (\$block_file_injections = 1) {
        return 403;
    }

    ## Block common exploits
    #set \$block_common_exploits 0;
    if (\$query_string ~ "(<|%3C).*script.*(>|%3E)") {
        set \$block_common_exploits 1;
    }
    if (\$query_string ~ "GLOBALS(=|\[|\%[0-9A-Z]{0,2})") {
        set \$block_common_exploits 1;
    }
    if (\$query_string ~ "_REQUEST(=|\[|\%[0-9A-Z]{0,2})") {
        set \$block_common_exploits 1;
    }
    if (\$query_string ~ "proc/self/environ") {
        set \$block_common_exploits 1;
    }
    if (\$query_string ~ "mosConfig_[a-zA-Z_]{1,21}(=|\%3D)") {
        set \$block_common_exploits 1;
    }
    if (\$query_string ~ "base64_(en|de)code\(.*\)") {
        set \$block_common_exploits 1;
    }
    if (\$block_common_exploits = 1) {
        return 403;
    }

    ## See the blacklist.conf file at the parent dir: /etc/nginx.
    ## Deny access based on the User-Agent header.
    if (\$bad_bot) {
        return 444;
    }
    ## Deny access based on the Referer header.
    if (\$bad_referer) {
        return 444;
    }

    ## Protection against illegal HTTP methods. Out of the box only HEAD,
    ## GET and POST are allowed.
    if (\$not_allowed_method) {
        return 405;
    }

    ## Filesystem root of the site and index.
    root /home/$DOMAIN/public;
    index index.php;

    ## If you're using a Nginx version greater or equal to 1.1.4 then
    ## you can use keep alive connections to the upstream be it
    ## FastCGI or Apache. If that's not the case comment out the line below.
    fastcgi_keep_conn on; # keep alive to the FCGI upstream

    include ${nginx_dir}/apps/drupal/drupal_boost.conf;
    #include ${nginx_dir}/apps/drupal/drupal_upload_progress.conf;

    ## Including the php-fpm status and ping pages config.
    include ${nginx_dir}/php_fpm_status_vhost.conf;

    ## Including the Nginx stub status page for having stats about
    ## Nginx activity: http://wiki.nginx.org/HttpStubStatusModule.
    include ${nginx_dir}/nginx_status_vhost.conf;

} 
EOF
) >  $config_dir/$DOMAIN.conf

echo "Making web directories"
mkdir -p $web_root/"$DOMAIN"
mkdir -p $web_root/"$DOMAIN"/public

ln -s $config_dir/"$DOMAIN".conf $enable_dir/"$DOMAIN".conf
service nginx restart
echo "Nginx - reload"
chown -R nginx:nginx $web_root/"$DOMAIN"
chmod 755 $web_root/"$DOMAIN"/public
echo "Permissions have been set"
echo "$DOMAIN has been setup"


# add directory to php restricted dir
inifile="/ect/php.ini"
basedir="/home/${DOMAIN}/public"
basedirline="$(grep "open_basedir =" /etc/php.ini)"
emptystring=';open_basedir = "'
if [ "$basedirline" == "$emptystring" ]
then
        echo "open_basedir directive is currently empty, uncommenting"
        newline='open_basedir = "'${basedir}'"'
else
        echo "adding site home to existing open_basedir"
        blength=`expr ${#basedirline} - 1`
        newline=${basedirline:0:blength}':'${basedir}'"'
fi
sed -i "s#${basedirline}#${newline}#i" /etc/php.ini

# create database
echo -e "Enter database short name:"
    read DBROOT

dbname=db"$DBROOT"
dbuser=usr"$DBROOT"
dbpass=$(</dev/urandom tr -dc A-Za-z0-9 | head -c 32)

Q1="CREATE DATABASE IF NOT EXISTS ${dbname};"
Q2="GRANT USAGE ON *.* TO ${dbuser}@localhost IDENTIFIED BY '${dbpass}';"
Q3="GRANT ALL PRIVILEGES ON ${dbname}.* TO ${dbuser}@localhost;"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"
echo "Require mysql root password to create database"
mysql -uroot -p -e "$SQL"

echo "User ${dbuser} created with password ${dbpass} for database ${dbname}"
